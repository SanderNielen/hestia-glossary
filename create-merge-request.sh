#!/usr/bin/env bash

echo "Commiting changes..."

cd $DEPLOY_REPO

git add --all
git commit -m "add changes from ${SOURCE_BRANCH}"
git push --force --set-upstream origin $SOURCE_BRANCH

echo "Creating MR..."

BODY="{
    \"id\": ${CI_PROJECT_ID},
    \"source_branch\": \"${SOURCE_BRANCH}\",
    \"target_branch\": \"${TARGET_BRANCH}\",
    \"assignee_id\": \"${GITLAB_USER_ID}\",
    \"remove_source_branch\": true,
    \"title\": \"WIP: merge ${SOURCE_BRANCH} onto ${TARGET_BRANCH}\"
}";

curl -X POST "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/merge_requests" \
    --header "PRIVATE-TOKEN:${GITLAB_CI_TOKEN}" \
    --header "Content-Type: application/json" \
    --data "${BODY}";
