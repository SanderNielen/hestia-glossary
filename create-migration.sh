#!/usr/bin/env bash

echo "Creating migration..."

cd $DEPLOY_REPO

git add --all
git diff --name-only origin/develop data/
git diff --output=../diff.diff origin/develop data/

cd ../

pip install unidiff pandas
python scripts/generate_migrations.py diff.diff $DEPLOY_REPO
