#!/bin/sh
docker build -t hestia-glossary:rscript -f Dockerfile.R .

docker run --rm \
  --name hestia-glossary-script \
  -v ${PWD}:/app \
  hestia-glossary:rscript Rscript "$@"
