| Key | Source |
| ------ | ------ |
| longFallowRatio | [Siebert, Portmann, and Doll. 2010. Global patterns of cropland use intensity](https://www.mdpi.com/2072-4292/2/7/1625) |
| croppingIntensity | [Siebert, Portmann, and Doll. 2010. Global patterns of cropland use intensity](https://www.mdpi.com/2072-4292/2/7/1625) |

<!--
Note: `GADM-USA` and `GADM-USA.12_1` values for `longFallowRatio` have been manually replaced with `1.4108`.
-->
