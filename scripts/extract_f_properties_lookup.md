# Extract properties from Feedipedia

## Usage

1. Install python `3.6` minimum
1. Install the required libraries: `pandas`, `requests` and `beautifulsoup4` (or run `pip install -r scripts/requirements.txt`)
1. Extract properties lookup first: `python scripts/convert_to_lookups.py`
1. Convert the desired terms: `python scripts/extract_f_properties_lookup.py Products/crop.xlsx`

This will create a `Products/crop-property-lookup.csv` file.
