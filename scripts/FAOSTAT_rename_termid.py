from os.path import dirname, join, abspath
import sys
import pandas as pd

CURRENT_DIR = dirname(abspath(__file__))

lookup = pd.read_csv(join(CURRENT_DIR, 'FAOSTAT_lookup_termid.csv'), index_col=None)


def main(args):
    file = args[0]
    should_drop = args[1].lower() == 'true' if len(args) > 1 else False
    df = pd.read_csv(file, index_col=None)

    drop_indexes = []

    for idx, row in df.iterrows():
        matching = lookup.loc[lookup['FAOSTAT Name'] == row['term.id']]
        if not matching.empty:
            term_id =  matching.to_dict('records')[0]['term.id']
            df.loc[df['term.id'] == row['term.id'], 'term.id'] = term_id
        else:
            drop_indexes.append(idx)

    if should_drop:
        df.drop(drop_indexes).to_csv(file, index=False)
    else:
        df.to_csv(file, index=False)


if __name__ == "__main__":
    main(sys.argv[1:])
