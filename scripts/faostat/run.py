import os
import traceback
import argparse
import json
from datetime import date
from functools import reduce
from statistics import mean
import urllib
import urllib.parse
import requests
from io import StringIO
import pandas as pd


API_URL = 'https://fenixservices.fao.org/faostat/api/v1'
LANG = 'en'
CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))
OUTPUT_FOLDER = os.path.join(CURRENT_DIR, '..', '..', 'Geographies')
DEFAULT_PARAMS = {
    "show_codes": "false",
    "show_unit": "false",
    "show_flags": "false",
    "show_notes": "false",
    "null_values": "false",
    "output_type": "csv",
    "datasource": "DB4"
}
FIX_GROUPING = {
    'Maté leaves': 'Mate leaves'
}

lookup_filepath = os.path.join(CURRENT_DIR, 'country-mapping.csv')
lookup = pd.read_csv(lookup_filepath, index_col=None)

available_types = [f.replace('.json', '') for f in os.listdir(CURRENT_DIR) if f.endswith('.json')]
available_term_types = ['crop', 'animalProduct', 'liveAnimal', 'inorganicFertiliser']

parser = argparse.ArgumentParser(description='Generating FAOSTAT data')
parser.add_argument('--type', type=str, required=True,
                    choices=available_types,
                    help='The type of data to generate.')
parser.add_argument('--term-type', type=str,
                    choices=available_term_types,
                    help='The termType of data to generate.')
parser.add_argument('--average-key', type=str,
                    help='The name of the key to create averages.')
args = parser.parse_args()


def is_emtpy(value): return str(value) == '-' or str(value) == '' or pd.isna(value)


def non_empty_value(value): return not is_emtpy(value)


def non_empty_list(values): return list(filter(non_empty_value, values))


def _load_config(name: str):
    with open(os.path.join(CURRENT_DIR, f"{name}.json"), 'r') as f:
        return json.load(f)


def _get_all_groupings(term_type: str, grouping: str):
    try:
        filepath = os.path.join(CURRENT_DIR, '..', '..', 'lookups', f"{term_type}.csv")
        df = pd.read_csv(filepath)
        return non_empty_list(list(df[grouping].unique()))
    except Exception:
        return []


def _mean(values: list):
    return mean(values) if len(values) > 1 else values[0] if len(values) == 0 else '-'


VALUE_BY_UNIT = {
    '1000 Head': lambda v: v * 1000,
    '0.1g/An': lambda v: v / 1000  # make sure units are all `hg/An`
}


def _parse_value(row: dict):
    value = row.get('Value')
    unit = row.get('Unit')
    return VALUE_BY_UNIT.get(unit, lambda v: v)(value)


def _term_id_from_name(name: str):
    value = lookup.loc[lookup['FAOSTAT Name'] == name]
    if not value.empty:
        return value.to_dict('records')[0]['term.id']
    else:
        print('Warning: matching "term.id" not found for', name, 'in', lookup_filepath)
        return None


def _term_id_from_mapping(itemMapping: dict): return lambda val: itemMapping.get(val, val)


def _group_by_country(itemMapping: dict):
    def exec(group: dict, row: dict):
        country_name = row.get('Area')
        term_id = _term_id_from_name(country_name)

        if term_id:
            grouping = _term_id_from_mapping(itemMapping)(row.get('Item'))
            group[term_id] = group.get(term_id, {})
            group[term_id][grouping] = group[term_id].get(grouping, {})
            group[term_id][grouping][row.get('Year')] = _parse_value(row)

        return group
    return exec


def _process_result(
    df: pd.DataFrame, file_suffix: str, groupings: list = [], itemMapping: dict = {},
    average_key: str = None, element_code: str = None
):
    years = sorted(list(df['Year'].unique()))

    rows = df.to_dict('records')
    # filter results by "Element Code"
    if element_code:
        rows = [r for r in rows if str(r.get('Element Code')) == str(element_code)]

    data = reduce(_group_by_country(itemMapping), rows, {})

    records = []
    for term_id, value in data.items():
        record = {'term.id': term_id}
        for grouping in groupings:
            year_values = value.get(grouping)
            if year_values:
                grouping_values = list(map(lambda year: f"{year}:{year_values.get(year, '-')}", years))
                grouping_values.extend([f"{average_key}:{_mean(year_values.values())}"] if average_key else [])
                record[grouping] = ';'.join(grouping_values)
            else:
                record[grouping] = '-'

        records.append(record)

    filepath = os.path.join(OUTPUT_FOLDER, f"{file_suffix}.csv")
    pd.DataFrame.from_records(records, index=['term.id']).to_csv(filepath)


def main():
    config = _load_config(args.type)
    key_config = config[args.term_type] if args.term_type else config.get('default', {})
    grouping = key_config.get('grouping', '')
    if 'grouping' in key_config:
        del key_config['grouping']

    itemMapping = key_config.get('itemMapping', {})
    if 'itemMapping' in key_config:
        del key_config['itemMapping']

    url_params = urllib.parse.urlencode({
        **DEFAULT_PARAMS,
        **config.get('default', {}),
        **key_config
    })
    url = f"{API_URL}/{LANG}/{config.get('url')}?{url_params}"
    print('Query:', url)
    result = requests.get(url).text
    df = pd.read_csv(StringIO(result))

    file_suffix = '-'.join(non_empty_list(['region', args.term_type, grouping, args.type, 'lookup']))

    try:
        # get the groupings from the lookup file (if exists), and merge with groupings from file
        groupings = sorted(set(
            (_get_all_groupings(args.term_type, grouping) if args.term_type else []) +
            list(map(_term_id_from_mapping(itemMapping), df['Item'].unique()))
        ))
        # fix grouping with encoding issues
        groupings = [FIX_GROUPING.get(v, v) for v in groupings]

        # note: to use `elementCode`, make sure `default.show_codes=true`
        element_code = config.get('elementCode')

        _process_result(df, file_suffix, groupings,
                        itemMapping=itemMapping, average_key=args.average_key, element_code=element_code)

        filepath = os.path.join(OUTPUT_FOLDER, f"{file_suffix}.md")
        with open(filepath, 'w') as f:
            f.write(f"**Source:** [FAOSTAT (2022)](https://www.fao.org/faostat/{LANG}/#{config.get('url')}), Accessed {date.today()}.")
    except Exception:
        stack = traceback.format_exc()
        print(stack)
        filepath = f"{file_suffix}-temp.csv"
        print('Saving raw data under', filepath)
        df.to_csv(filepath)


if __name__ == "__main__":
    main()
