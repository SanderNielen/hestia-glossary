// script to make sure all files have a `.md` file associated with (documentation)
const { readdirSync, lstatSync } = require('fs');
const { resolve, join } = require('path');

const ROOT = resolve(join(__dirname, '..'));
const ignoreFolders = [
  'csv',
  'data',
  'gadm',
  'lookups',
  'node_modules',
  'resources',
  'scripts'
];

const run = () => {
  const folders = readdirSync(ROOT).filter(f => [
    lstatSync(f).isDirectory(),
    !f.startsWith('.'),
    !ignoreFolders.includes(f)
  ].every(Boolean)).map(f => join(ROOT, f));
  const allFiles = folders.flatMap(folder => readdirSync(folder));
  const files = allFiles.filter(f => f.endsWith('.csv') || f.endsWith('.xlsx'));
  const missingMarkdowns = files.filter(f => !allFiles.includes(f.replace('.csv', '.md').replace('.xlsx', '.md')));

  if (missingMarkdowns.length) {
    console.error(`The following files are missing documentation (".md" file with same name):\n\t${missingMarkdowns.join('\n\t')}`);
    process.exit(1);
  }
};

run();
