' PubChem and ChemIDPlus provide data on pesticide active ingredients
' These data are accessible using the CAS number of each chemical
' However, sometimes these URLs fail
' This script tests whether the URL works by trying to access the <title> object
' The title object is empty if PubChem fails and displays an error for ChemIDPlus

Function GetTitleFromURL(sURL As String)
	Dim wb As Object
	Dim doc As Object

	Set wb = CreateObject("InternetExplorer.Application")

	wb.Navigate sURL

' Chem id has a 3 second time out, so add a delay
	If Left(sURL, 24) = "https://chem.nlm.nih.gov" Then Pause (4)

	While wb.Busy
		DoEvents
	Wend

	GetTitleFromURL = wb.Document.Title


	wb.Quit

	Set wb = Nothing
End Function


Public Function Pause(NumberOfSeconds As Variant)
	On Error GoTo Error_GoTo

	Dim PauseTime As Variant
	Dim Start As Variant
	Dim Elapsed As Variant

	PauseTime = NumberOfSeconds
	Start = Timer
	Elapsed = 0
	Do While Timer < Start + PauseTime
		Elapsed = Elapsed + 1
		If Timer = 0 Then
            ' Crossing midnight
			PauseTime = PauseTime - Elapsed
			Start = 0
			Elapsed = 0
		End If
		DoEvents
	Loop

	Exit_GoTo:
	On Error GoTo 0
	Exit Function
	Error_GoTo:
	Debug.Print Err.Number, Err.Description, Erl
	GoTo Exit_GoTo
End Function
