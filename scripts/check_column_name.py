import os
import sys
import re
from functools import reduce
import pandas as pd
from hestia_earth.schema import Term

EXTENSION = '.xlsx'
BASE_DIR = './'
EXCLUDED_FOLDERS = ['csv', 'data', 'gadm', 'lookups', 'envs', 'scripts']

term = Term()
term_fields = term.fields.keys()
term_regex = re.compile(r'^term\.[A-Za-z]+')
lookup_regex = re.compile(r'^lookups\.[\d]+\.(name|value|sd|min|max|source|notes|proxy\.id|dataState)$')


def flatten(values: list): return list(reduce(lambda x, y: x + (y if isinstance(y, list) else [y]), values, []))


def _list_files(folder: str):
    return list(map(
        lambda filename: os.path.join(folder, filename),
        list(filter(lambda file: file.endswith(EXTENSION), os.listdir(folder)))
    ))


def _valid_term_column(column: str):
    return all([
        column.split('.')[1] in term_fields,
        term_regex.match(column)
    ])


def _valid_lookup_column(column: str):
    return lookup_regex.match(column)


def _valid_column(column: str):
    return any([
        column.startswith('term.') and _valid_term_column(column),
        column.startswith('lookups.') and _valid_lookup_column(column),
        column == '-' or column.startswith('-.'),
    ])


def _validate_file(filepath: str):
    df = pd.read_excel(filepath, index_col='term.id')
    columns = list(df.columns)
    return [c for c in columns if not _valid_column(c)]


def main():
    folders = [
        os.path.join(BASE_DIR, f) for f in os.listdir(BASE_DIR) if f not in EXCLUDED_FOLDERS and os.path.isdir(f)
    ]
    files = flatten(map(_list_files, folders))

    exit_code = ''

    for filepath in files:
        errors = _validate_file(filepath)
        if len(errors) > 0:
            err = '\n\t'.join(errors)
            exit_code += f"\nError in column names in '{filepath}':\n\t{err}"

    if exit_code != '':
        exit_code += f"\nPlease make sure all columns are valid Term fields or lookups, or contain only '-'"

    sys.exit(0 if exit_code == '' else exit_code)


if __name__ == "__main__":
    main()
