require('dotenv').config();
const axios = require('axios');

const [termType] = process.argv.slice(2);

const API_URL = process.env.API_URL;
const JSON_LD_API_URL = process.env.JSON_LD_API_URL;
const JSON_LD_API_KEY = process.env.JSON_LD_API_KEY;
const limit = 100;

const findTerms = async () => {
  const { data: { results } } = await axios.post(`${API_URL}/search`, {
    limit,
    fields: ['@type', '@id'],
    query: {
      bool: {
        must: [
          {
            match: { '@type': 'Term' }
          },
          {
            match: { termType }
          }
        ]
      }
    }
  });
  return results;
};

const deleteNodes = (nodes) => axios.delete(`${JSON_LD_API_URL}/node`, {
  headers: { 'x-api-key': JSON_LD_API_KEY },
  data: { nodes }
});

const deleteTerms = async () => {
  const terms = await findTerms();
  console.log('Deleting', terms.length, 'terms.');
  await deleteNodes(terms);
  return terms.length === limit ? await deleteTerms() : null;
};

const run = () => deleteTerms();

run().then(() => process.exit(0)).catch(err => {
  console.error(err);
  process.exit(1);
});
