import sys
import pandas as pd


def main(args):
    file1 = pd.read_csv(args[0], index_col=0)
    file2 = pd.read_csv(args[1], index_col=0)

    for id, row in file1.iterrows():
        try:
            row2 = file2.loc[id]
            if row2 is None or row2.empty:
                print('not found', id)
        except Exception:
            print('not found', id)


if __name__ == "__main__":
    main(sys.argv[1:])
