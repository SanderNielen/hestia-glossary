# extract properties from feedipedia
import sys
import requests
import pandas as pd
from bs4 import BeautifulSoup


PROPERTY_NAME = 'feedipediaName'
DM_PROP = 'Dry matter'
SKIP_COLS = ['', 'Unit', 'Nb']


def non_empty_value(value): return str(value) != '-' and str(value) != '' and pd.notna(value)


def float_precision(value): return "{:.5f}".format(float(value) if type(value) == str else value)


def default_dm_value(term):
    value = term['term.defaultProperties.0.value']
    return float(value) if value and value != '-' else 0


def row_to_list(row):
    def td_content(td):
        try:
            return td.a.string.strip()
        except Exception:
            return td.string.strip() if td.string is not None else ''

    return [td_content(td) for td in row.find_all('td') if td is not None]


def clean_value(value):
    return value.replace('*', '')


CONVERT_PROP = {
    'Phosphorus': lambda value, dm_value: float_precision(float(value) * (dm_value / 100) / 10) if dm_value else '',
    'Potassium': lambda value, dm_value: float_precision(float(value) * (dm_value / 100) / 10) if dm_value else '',
    'Energy digestibility, ruminants': lambda value, dm_value: float_precision(value)
}


def convert_property_value(property_name: str, col_name: str, value, dm_value: float):
    should_convert = col_name in ['Avg', 'Min', 'Max', 'SD'] and value != ''
    return CONVERT_PROP[property_name](value, dm_value) if property_name in CONVERT_PROP and should_convert else (
        (
            float_precision(float(value) * (dm_value / 100)) if dm_value else ''
        ) if property_name != DM_PROP and should_convert else value
    )


def find_property(tables, property_name: str):
    for table in tables:
        rows = table.find_all('tr')
        first_row = rows[0]
        if 'tableheader' in first_row.attrs.get('class', []):
            headers = row_to_list(first_row)
            for row in rows[1:]:
                cols = row_to_list(row)
                prop_name = cols[0]
                if prop_name == property_name:
                    for index, col in enumerate(cols[1:]):
                        if col != '' and len(headers) > index + 1 and headers[index + 1] == 'Avg':
                            return float(clean_value(col))
    return 0


def extract_term(term, url: str, properties: dict, default_dm_value: float):
    page = requests.get(url)
    soup = BeautifulSoup(page.text, 'html.parser')

    # make sure the url points to a correct page
    has_field_content = len(soup.find_all('div', {'class': 'field-content'}))
    if has_field_content:
        return None

    tables = soup.find_all('table')

    term_id = term['term.id']
    row_dict = {'term.id': term_id}
    dm_value = find_property(tables, DM_PROP) or default_dm_value
    if not dm_value:
        print('Warning: missing dry matter property and table value for', term['term.id'])

    for table in tables:
        rows = table.find_all('tr')
        first_row = rows[0]
        if 'tableheader' in first_row.attrs.get('class', []):
            headers = row_to_list(first_row)
            for row in rows[1:]:
                cols = row_to_list(row)
                prop_name = cols[0]
                matching_properties = [key for key, value in properties.items() if value == prop_name]
                if len(matching_properties) > 0:
                    prop_values = []
                    for index, col in enumerate(cols[1:]):
                        if col != '' and len(headers) > index + 1 and headers[index + 1] not in SKIP_COLS:
                            col_name = headers[index + 1]
                            value = convert_property_value(prop_name, col_name, clean_value(col), dm_value)
                            if value != '':
                                prop_values.append(':'.join([col_name, str(value)]))

                    # multiple properties can have the same name, add them all
                    for prop_id in matching_properties:
                        row_dict[prop_id] = ';'.join(prop_values)

    return row_dict, dm_value


def extract(src_file: str):
    src_df = pd.read_excel(src_file)
    properties_df = pd.read_csv('lookups/property.csv', index_col='term.id')
    properties = properties_df[PROPERTY_NAME].dropna().to_dict()
    rows = []
    errors = []
    for index, term in src_df.iterrows():
        prop = term['term.feedipedia']
        if non_empty_value(prop):
            dm_value = default_dm_value(term)
            try:
                row_dict, dm_value = extract_term(term, prop, properties, dm_value)
                if row_dict:
                    rows.append(row_dict)
                else:
                    print(f"{term['term.id']} / {prop} does not contain the correct content. Please verify it points to a single factsheet.")
            except Exception as e:
                errors.append(f"Error for {term['term.id']} (link={prop}): {str(e)}")

    dest_file = src_file.replace('.xlsx', '-property-lookup.csv')
    pd.DataFrame.from_records(rows).to_csv(dest_file, index=False, na_rep='')

    exit_code = '\n'.join(errors) if len(errors) > 0 else 0
    sys.exit(exit_code)


def main(args):
    src_file = args[0]
    extract(src_file)


if __name__ == "__main__":
    main(sys.argv[1:])
