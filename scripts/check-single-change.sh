#!/usr/bin/env bash

set -e

COMMIT_SHA="$1"
FILE="$2"

DIFFS=$(git diff --name-only $COMMIT_SHA origin/develop :!scripts/* :!*-property-lookup.csv) || ""
COUNT=$(git diff --name-only $COMMIT_SHA origin/develop :!scripts/* :!*-property-lookup.csv | wc -l) || 1
if [[ "$DIFFS" == *"$FILE"* ]]; then
  if [ "$COUNT" -gt 1 ]; then
    echo "Multiple changes forbidden when making changes to ${FILE}." 1>&2
    echo "Please create a Merge Request including only changes on ${FILE}." 1>&2
    exit 1
  fi
fi
