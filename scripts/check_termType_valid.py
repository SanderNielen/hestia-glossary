# Make sure all the `termType` used actually exist
import sys
import os
from functools import reduce
from hestia_earth.schema import TermTermType

EXTENSION = '.xlsx'
BASE_DIR = './'
EXCLUDED_FOLDERS = ['csv', 'data', 'envs', 'scripts']
TERM_TYPES = [e.value for e in TermTermType]


def flatten(values: list): return list(reduce(lambda x, y: x + (y if isinstance(y, list) else [y]), values, []))


def find_files(folder: str):
    folderpath = os.path.join(BASE_DIR, folder)
    if os.path.isdir(folderpath):
        return [f.replace(EXTENSION, '') for f in os.listdir(folderpath) if f.endswith(EXTENSION) and '-' not in f]
    return []


def main(args: list):
    folders = args[0].split(',') if len(args) > 0 \
        else list(filter(lambda f: f not in EXCLUDED_FOLDERS, os.listdir(BASE_DIR)))
    files = flatten(list(map(find_files, folders)))

    exit_code = ''

    for file in files:
        if file not in TERM_TYPES:
            exit_code += f"\nInvalid termType: {file}"

    sys.exit(0 if exit_code == '' else exit_code)


if __name__ == "__main__":
    main(sys.argv[1:])
