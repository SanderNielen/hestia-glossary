import os
import sys
from typing import List
from functools import reduce
from concurrent.futures import ThreadPoolExecutor
import pandas as pd

DATA_DIR = 'csv'
LOOKUP_DIR = 'lookups'
EXCLUDED_FOLDERS = ['csv', 'data', 'gadm', 'envs', 'scripts']
COLUMNS = [
    'defaultModelId',
    'productTermIdsAllowed',
    'mustIncludeId',
    'sensitivityAlternativeTerms',
    'fuelUse',
    'inputProductionGroupId',
    'liveAnimal',
    'liveweightPerHead',
    'weightAtMaturity',
    'animalProductId',
    'grazedPastureGrassInputId',
    'animalProductGroupingEquivalentProperty',
    'animalProductGroupingFAOEquivalent',
    'excretaKgMassTermId', 'excretaKgNTermId', 'excretaKgVsTermId',
    'allowedExcretaKgMassTermIds', 'allowedExcretaKgNTermIds', 'allowedExcretaKgVsTermIds',
    'recommendedExcretaKgMassTermIds', 'recommendedExcretaKgNTermIds', 'recommendedExcretaKgVsTermIds',
    'digestibility', 'digestibleEnergy',
    'primaryMeatProductFAO',
    'allowedRiceTermId'
]


def _flatten(values: list): return list(reduce(lambda x, y: x + (y if isinstance(y, list) else [y]), values, []))


def _is_emtpy(value): return str(value) == '-' or str(value) == '' or str(value) == 'nan' or pd.isna(value)


def _non_empty_value(value): return not _is_emtpy(value)


def _non_empty_list(values): return list(filter(_non_empty_value, values))


def _get_files(folder: str):
    return list(map(
        lambda filename: os.path.join(folder, filename),
        list(filter(lambda file: file.endswith('.csv'), os.listdir(folder)))
    ))


def _get_all_files(folder: str):
    folders = [os.path.join(folder, f) for f in os.listdir(folder) if os.path.isdir(os.path.join(folder, f))]
    return list(reduce(lambda prev, curr: prev + _get_files(curr), folders, []))


def _get_all_ids():
    files = _get_all_files(DATA_DIR)
    return reduce(lambda prev, curr: prev + pd.read_csv(curr)['id'].tolist(), files, [])


def _extract_ids(value: str):
    # handle "id1:10;id2:100"
    values = value.split(';') if ';' in value else [value]
    return [v.split(':')[0] for v in values]


def _check_lookup_terms(files: list, term_ids: list, column: str, ignore_values=['all', 'none', '-']):
    def check_file(filepath: str):
        df = pd.read_csv(filepath)
        columns = [col for col in df if col == column]
        values = _non_empty_list(set(_flatten([
            _extract_ids(str(x)) for x in set().union(*df[columns].values.tolist())
        ])))
        ids = [str(x) for x in values if str(x) not in (term_ids + ignore_values)]
        return filepath, ids

    with ThreadPoolExecutor() as executor:
        return list(executor.map(check_file, files))


def main(args: List[str]):
    files = _get_files(LOOKUP_DIR)

    term_ids = _get_all_ids()
    exit_code = ''

    for column in COLUMNS:
        for filepath, ids in _check_lookup_terms(files, term_ids, column):
            if len(ids) > 0:
                iids = '\n\t'.join(ids)
                exit_code += f"\nfound {len(ids)} non-existing '{column}' in '{filepath}':\n\t{iids}"

    sys.exit(0 if exit_code == '' else exit_code)


if __name__ == "__main__":
    main(sys.argv[1:])
