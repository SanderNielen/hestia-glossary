import os
import sys
import time
from io import StringIO
import pandas as pd
from unidiff import PatchSet


def current_time_ms(): return str(int(time.time() * 1000))


def _parse_line(line: str):
    # e.g. `-termId,"term name",units`
    try:
        df = pd.read_table(StringIO(line[1:]), sep=',')
        return list(df.columns.values)
    except Exception:
        return line[1:].split(',')


def escape_field(value):
    val = str(value).replace('"', '""').replace('\r', '').replace('\n', '')
    return f"\"{val}\"" if ',' in val or '"' in val else val


def main(args: list):
    diff_file = args[0]
    dest_folder = args[1]
    patches = PatchSet.from_filename(diff_file)
    print('nb files', len(patches))

    term_ids = []
    term_names = {}
    for patch in patches:
        for hunk in patch:
            for line in hunk:
                if line.is_removed:
                    old_term_id, old_term_name, *_ = _parse_line(str(line))
                    term_ids.append(old_term_id)
                    term_names[old_term_id] = {'old': old_term_name}

    # make sure we skip updated terms
    for patch in patches:
        for hunk in patch:
            for line in hunk:
                if line.is_added:
                    new_term_id, new_term_name, *_ = _parse_line(str(line))
                    # term id was removed and added - skip
                    if new_term_id in term_ids:
                        term_ids.remove(new_term_id)
                    old_term_name = term_names.get(new_term_id, {}).get('old')
                    if old_term_name:
                        # term name has not been changed
                        if old_term_name == new_term_name:
                            del term_names[new_term_id]
                        else:
                            term_names[new_term_id]['new'] = new_term_name

    print('nb term ids', len(term_ids))
    if len(term_ids) > 0 or term_names:
        filepath = os.path.join(dest_folder, 'migrations', f"{current_time_ms()}.csv")
        open(filepath, 'w+').writelines('oldTermId,newTermId,instructions,oldTermName,newTermName\n')

        for term_id in term_ids:
            name_mapping = term_names.get(term_id, {})
            open(filepath, 'a+').writelines(','.join([
                escape_field(term_id), '', '',
                escape_field(name_mapping.get('old', '')),
                f"{escape_field(name_mapping.get('new', ''))}\n"
            ]))

        # add all name changes not linked to a change in term id
        for term_id, name_mapping in term_names.items():
            if term_id not in term_ids:
                open(filepath, 'a+').writelines(','.join([
                    '', '', '',
                    escape_field(name_mapping.get('old')),
                    f"{escape_field(name_mapping.get('new', ''))}\n"
                ]))

if __name__ == "__main__":
    main(sys.argv[1:])
