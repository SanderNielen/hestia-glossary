# updates the names based on duplicates in the region.xlsx file
# deps: pip3 install pandas openpyxl
from os.path import dirname, join, abspath
import sys
import pandas as pd

CURRENT_DIR = dirname(abspath(__file__))
SRC_DIR = join(CURRENT_DIR, '..', 'Geographies')
filename = 'region.xlsx'


def non_empty_list(values: list): return list(filter(lambda x: len(x) > 0, values))


def main(args):
    duplicates_file = args[0] if len(args) > 0 else join(CURRENT_DIR, '..', 'gadm', 'duplicates.txt')

    with open(duplicates_file, 'r') as f:
        duplicates = non_empty_list(f.read().split('\n'))

    df = pd.read_excel(join(SRC_DIR, filename))

    for duplicate in duplicates:
        new_value = df[df['term.id'] == duplicate]['term.gadmFullName']
        df.loc[df['term.id'] == duplicate, 'term.name'] = new_value

    df.to_excel(join(SRC_DIR, filename), index=None, header=True)


if __name__ == "__main__":
    main(sys.argv[1:])
