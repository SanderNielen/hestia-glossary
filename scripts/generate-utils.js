const { mkdirSync, writeFileSync, readdirSync } = require('fs');
const { resolve, join } = require('path');
const csv = require('csvtojson/v2');

const rootDir = resolve(join(__dirname, '..'));
const outputDir = resolve(join(rootDir, 'resources'));
const lookupsDir = join(rootDir, 'lookups');

const ensureDir = (dir) => {
  try { mkdirSync(dir, { recursive: true }) } catch (err) { }
};

const listLookups = () => readdirSync(lookupsDir);

const readLookup = async file => await csv().fromFile(join(lookupsDir, file));

const writeJson = (filename, data) => {
  ensureDir(outputDir);
  return writeFileSync(join(outputDir, filename), JSON.stringify(data, null, 2), 'utf8');
};

module.exports = {
  listLookups,
  readLookup,
  writeJson
};
