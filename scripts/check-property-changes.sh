#!/usr/bin/env bash

set -e

COMMIT_SHA="$1"
TERM_TYPE="$2"
SRC_FILE="Products/${TERM_TYPE}.xlsx"
PROP_FILE="Products/${TERM_TYPE}-property-lookup.csv"

DIFFS=$(git diff --name-only $COMMIT_SHA origin/develop) || ""
if [[ "$DIFFS" == *"$SRC_FILE"* ]] || [[ "$DIFFS" == *"$PROP_FILE"* ]] || [[ "$DIFFS" == *"Properties/property.xlsx"* ]]; then
  echo "Exctracting properties..."

  # compute property file
  # result will contain information if there are warnings
  RES=$(python scripts/extract_f_properties_lookup.py "$SRC_FILE")

  # check diffs again
  DIFFS=$(git diff --name-only) || ""
  if [[ "$DIFFS" == *"$PROP_FILE"* ]]; then
    echo "Changes have been detected in ${PROP_FILE}." 1>&2
    echo "Please check https://gitlab.com/hestia-earth/hestia-glossary/-/blob/develop/scripts/extract_f_properties_lookup.md for instructions and commit changes." 1>&2
    echo "This script automatically gets data from Feedipedia for key ${TERM_TYPE} data (like nutrient contents) so is critical to run if new ${TERM_TYPE} are added." 1>&2
    exit 1
  fi

  if [[ $RES ]]; then
    echo "Extraction of properties finished with warnings:"
    echo "$RES"
    exit 2
  fi
fi
