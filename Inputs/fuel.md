**Sources:** [International Recommendations for Energy Statistics](https://unstats.un.org/unsd/energystats/methodology/ires/); [Engineering Toolbox](https://www.engineeringtoolbox.com); Hestia Team.
