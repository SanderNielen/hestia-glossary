# updates the synonyms in the region.csv file
# deps: pip3 install unidecode openpyxl
import sys
import pandas as pd
from unidecode import unidecode


def name_synonym(name: str):
    synonym = unidecode(name)
    return None if synonym == name else synonym


def update_synonyms(name, synonyms):
    synonym = name_synonym(name) if pd.notna(name) else None
    synonyms = synonyms.split(';') if pd.notna(synonyms) else []
    return ';'.join(list(set(synonyms + [synonym]))) if synonym else ''


def update_row(row): return update_synonyms(row['term.gadmName'], row['term.synonyms'])


def main(args):
    regions = pd.read_csv(args[0], index_col=None, encoding='utf-8')
    regions['term.synonyms'] = regions.apply(update_row, axis=1)
    regions.to_csv(args[1], index=None, header=True, encoding='utf-8')


if __name__ == "__main__":
    main(sys.argv[1:])
