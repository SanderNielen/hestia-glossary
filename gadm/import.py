# run pip install shapely
import os
import argparse
import traceback
from functools import reduce
from concurrent.futures import ThreadPoolExecutor
import geopandas as gp
import pandas as pd
from unidecode import unidecode


parser = argparse.ArgumentParser()
parser.add_argument('--version', type=int, default=40,
                    help='Current GADM version')
parser.add_argument('--code', type=str,
                    help='The 3-letter GADM country code')
parser.add_argument('--level', type=int,
                    help='0 to 5 - run for specific level only')
parser.add_argument('--skip-merge-files', action='store_true',
                    help='By default, all generated files will be merged into a single file. Add to skip.')
parser.add_argument('--skip-existing', action='store_true',
                    help='Skip existing generated country files.')
parser.add_argument('--limit', type=int,
                    help='Limit the number of files to handle. Only works if --code is not set.')
parser.add_argument('--offset', type=int, default=0,
                    help='Combine with --limit to paginate through the files. Only works if --code is not set.')
args = parser.parse_args()


def mkdirs(dest: str):
    if not os.path.isdir(dest):
        os.makedirs(dest)


INPUT_DIR = 'files'
OUTPUT_DIR = 'data'
mkdirs(OUTPUT_DIR)
# gadm Ids which name is "None"
NONE_IDS = [
    'GADM-ITA.13.6.168_1'
]

mapping = pd.read_csv('mappings.csv', index_col=None)
MAPPING_COLUMN = 'gadmId'


def flatten(values: list): return list(reduce(lambda x, y: x + (y if isinstance(y, list) else [y]), values, []))


def non_empty_list(values: list): return list(filter(lambda x: len(x) > 0, values))


def paginate(values: list, offset=0, limit=None): return values[offset:(limit + offset if limit is not None else None)]


with open('./duplicates.txt', 'r') as f:
    duplicates = non_empty_list(f.read().split('\n'))


with open('./ignore.txt', 'r') as f:
    ignore_ids = non_empty_list(f.read().split('\n'))


def extend_from_mapping(name: str):
    try:
        return mapping.loc[mapping[MAPPING_COLUMN] == name].to_dict('records')[0]
    except IndexError:
        return {}


def escape_field(value):
    val = str(value).replace('"', '""').replace('\r', '').replace('\n', '')
    return f"\"{val}\"" if ',' in val or '"' in val else val


def col_key_value(key: str, values: dict, default=''):
    return [key, values[key]] if key in values and pd.notna(values[key]) else ['', default]


def col_value(key: str, values: dict, default=''):
    return values[key] if key in values and pd.notna(values[key]) else default


def sub_class_of(gid: str):
    parts = gid.split('.')
    if len(parts) == 1:
        return None
    id = parts.pop().split('_')[1]
    return f"{'.'.join(parts)}_{id}" if len(parts) >= 2 else parts[0]


FORMAT_TYPE_BY_COUNTRY = {
    'Italy': lambda v: v.replace('Commune', 'Comune')
}


def format_type(level: int, row: dict):
    country_name = row['COUNTRY']
    has_data = f"TYPE_{level}" in row
    return FORMAT_TYPE_BY_COUNTRY.get(country_name, lambda v: v)(row[f"TYPE_{level}"]) if has_data else None


def full_name(level: int, row: dict):
    parts = []
    while level >= 0:
        gtype = format_type(level, row)
        column = f"NAME_{level}" if level > 0 else 'COUNTRY'
        parts.extend([row[column] + (f" ({gtype})" if gtype else '')] if row[column] else [])
        level = level - 1
    return ', '.join(parts)


def name_synonym(name: str):
    synonym = unidecode(name)
    return '' if synonym == name else synonym


def row_to_csv(level: int, row: dict):
    gid = row['ID_' + level]
    original_id = 'GADM-' + gid
    gname = row['NAME_' + level if int(level) > 0 else 'COUNTRY']
    name = full_name(int(level), row)
    country_name = row['COUNTRY']
    centroid = gp.GeoSeries(row['geometry']).centroid[0].coords[0]
    latitude = centroid[1]
    longitude = centroid[0]
    geoserie = gp.GeoSeries(row['geometry'], crs='epsg:4326').to_crs(epsg=6933)
    area = geoserie.area[0] / 10**6
    extensions = extend_from_mapping(original_id)
    sub_class_id = sub_class_of(original_id) or col_value('subClassOf', extensions)
    synonym = (row['VARNAME_' + level] if 'VARNAME_' + level in row else None) or ''
    ext_synonyms = col_value('synonyms', extensions)
    synonyms = ';'.join(non_empty_list(list(set(synonym.split('|') + [ext_synonyms, name_synonym(gname)]))))
    original_name = col_value('name', extensions, gname)
    # do not add if no name, no ID or set to be ignored
    add_to_list = gname is not None and (gname != 'None' or original_id in NONE_IDS) and original_id not in ignore_ids
    return ','.join(list(map(
        escape_field,
        [
            original_id,
            name if original_id in duplicates else original_name,  # handle duplicate names in Glossary
            gid,
            gname,
            name,
            level,
            country_name,
            col_value('iso31662Code', extensions),
            latitude,
            longitude,
            synonyms,
            sub_class_id or '',
            area
        ]
    ))) if add_to_list else None


def handle_file(file: dict):
    try:
        zones = gp.read_file(file.get('input'), layer=file.get('layer'), encoding='utf-8')
        print('generating', file.get('output'))
        #  try removing file first
        try:
            os.remove(file.get('output'))
        except Exception:
            True

        open(file.get('output'), 'w', encoding='utf-8').close()
        for index, row in zones.iterrows():
            try:
                row_csv = row_to_csv(str(file.get('level')), row)
                if row_csv is not None:
                    open(file.get('output'), 'a+', encoding='utf-8').writelines(row_csv + '\n')
            except Exception:
                print(traceback.format_exc())
        return file.get('output')
    except ValueError:
        return ''


def file_args(code: str, level):
    dest_file = f"{OUTPUT_DIR}/{code}_{level}.csv"
    filepath = f"zip://{INPUT_DIR}/gadm{args.version}_{code}_shp.zip"
    layer = f"gadm{args.version}_{code}_{level}"
    return {
        'input': filepath,
        'output': dest_file,
        'layer': layer,
        'code': code,
        'level': level
    }


def compute_files():
    levels = [args.level] if args.level is not None else [0, 1, 2, 3, 4, 5]
    files = list(map(lambda x: f"gadm{args.version}_{x}_shp.zip", args.code.split(','))) if args.code \
        else paginate(sorted(os.listdir(f"{INPUT_DIR}")), args.offset, args.limit)
    print('importing files', files)
    codes = list(map(lambda f: f.replace(f"gadm{args.version}_", '').replace('_shp.zip', ''), files))
    files = []
    for code in codes:
        for level in levels:
            fargs = file_args(code, level)
            skip_level = args.skip_existing and os.path.exists(fargs.get('output'))
            files.extend([] if skip_level else [fargs])
    return files


def merge_files(files: list):
    filename = args.code or 'region'

    with open('./headers.txt', 'r') as f:
        headers = non_empty_list(f.read().split('\n'))

    with open(f"{OUTPUT_DIR}/{filename}.csv", 'w', encoding='utf-8') as outfile:
        outfile.write(','.join(headers) + '\n')
        for fname in files:
            with open(fname) as infile:
                outfile.writelines(infile.readlines())
        with open('additional-regions.csv') as missing:
            outfile.writelines(missing.readlines())


def main():
    files = compute_files()

    with ThreadPoolExecutor() as executor:
        all_files = list(executor.map(handle_file, files))

    all_files = non_empty_list(all_files)
    all_files.sort(key=lambda x: int(x[9:].replace('.csv', '')))
    return merge_files(all_files) if not args.skip_merge_files else None


if __name__ == "__main__":
    main()
