#!/bin/sh
# npm install -g simplify-geojson

INPUT_DIR=${1:-"data"}
OUTPUT_DIR=${2:-"data"}

total=$(find $INPUT_DIR/*.geojson -maxdepth 0 | wc -l)
i=1

for file in $INPUT_DIR/*.geojson; do
  DEST=${file/INPUT_DIR/OUTPUT_DIR}
  RES="$(cat $file | simplify-geojson -t 0.01)"
  echo $RES > $DEST
  remaining=$(($total-$i))
  echo "$remaining files remaining"
  ((i++))
done
