# find the broken files.
# deps: pip3 install python-magic
import os
import re
import magic


VALID_ENCODING = [
  'utf-8',
  'us-ascii'
]


def valid_encoding(file):
    blob = open(f"data/{file}", 'rb').read()
    m = magic.Magic(mime_encoding=True)
    return m.from_buffer(blob) not in VALID_ENCODING


def group_files(files):
    group = {}
    for file in files:
        [code, level] = file.replace('.csv', '').split('_')
        group[level] = [] if level not in group else group[level]
        group[level].append(code)
    return group


file_regex = re.compile(r'[A-Z]{3}_[\d]+\.csv')
files = os.listdir('data')
files = list(filter(lambda f: file_regex.match(f), files))
files = list(filter(valid_encoding, files))
for level, codes in group_files(files).items():
    print(f"python3 import.py --code={','.join(codes)} --level={level} --skip-merge-files;")
