const csv = require('csvtojson/v2');
const { mkdirSync, writeFileSync } = require('fs');
const { resolve, join } = require('path');

const outputDir = resolve(join(__dirname, '../', 'resources'));

const ensureDir = (dir) => {
  try { mkdirSync(dir, { recursive: true }) } catch (err) { }
};

ensureDir(outputDir);

const groupBy = (data, key, sourceKey) => data
  .filter(v => v[key])
  .reduce((prev, curr) => ({ ...prev, [curr[key]]: curr[sourceKey] }), {});

const run = async () => {
  const data = await csv().fromFile(resolve(join(__dirname, 'mappings.csv')));
  writeFileSync(join(outputDir, 'iso3166-2-to-name.json'), JSON.stringify(groupBy(data, 'iso31662Code', 'gadmName'), null, 2), 'utf8');
  writeFileSync(join(outputDir, 'name-to-iso3166-2.json'), JSON.stringify(groupBy(data, 'gadmName', 'iso31662Code'), null, 2), 'utf8');
  writeFileSync(join(outputDir, 'iso3166-2-to-id.json'), JSON.stringify(groupBy(data, 'iso31662Code', 'gadmId'), null, 2), 'utf8');
  writeFileSync(join(outputDir, 'id-to-iso3166-2.json'), JSON.stringify(groupBy(data, 'gadmId', 'iso31662Code'), null, 2), 'utf8');
};

run().then(() => process.exit(0)).catch(err => {
  console.error(err);
  process.exit(1);
});
