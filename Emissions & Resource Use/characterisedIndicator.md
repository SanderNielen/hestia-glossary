**Sources:** Hestia Team; [OpenLCA](https://www.openlca.org/); [ecoinvent](https://ecoinvent.org/).

**Note:** Includes both mid-point and end-point characterised indicators.
